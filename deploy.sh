echo "Ready to deploy"
which ssh-agent 
eval $(ssh-agent -s)
ssh-add <(echo "$1")
mkdir -p ~/.ssh
[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
ssh root@$2 "docker run -d prom/prometheus -p 9090:9090"

